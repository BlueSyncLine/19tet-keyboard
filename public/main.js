var C0 = 132.0
var NOTE_NAMES = ["C", "Cp", "C#", "D", "Dp", "D#", "E", "Ep", "F", "F#", "F#p", "G", "Gp", "G#", "A", "Ap", "A#", "B", "Bp"];

var audioCtx, osc, playing, textArea;


function startNote(n) {
    osc = audioCtx.createOscillator();
    osc.type = "triangle";
    osc.frequency.value = C0 * Math.pow(2, n / 19.0);
    osc.connect(audioCtx.destination);
    osc.start();
}

function buttonClicked(n, up) {
    if (!up && !playing) {
        startNote(n);
        playing = true;
        var octave = Math.floor(n / 19);
        textArea.value += NOTE_NAMES[n % 19] + octave + " ";
    } else if (up && playing) {
        osc.stop();
        playing = false;
    }
}

function load() {
    audioCtx = new AudioContext();
    playing = false;

    function makeButtonHandler(n, up) {
        return function() {buttonClicked(n, up);};
    }

    for (var i = 0; i < 19 * 3; i++) {
        var button = document.createElement("button");
        button.innerText = NOTE_NAMES[i % 19];
        button.onmousedown = makeButtonHandler(i, false);
        button.onmouseup = makeButtonHandler(i, true);
        button.onmouseout = button.onmouseup;

        if (button.innerText.length > 1)
            button.style.color = "#808080";

        document.body.appendChild(button);

        if (i > 0 && i % 19 == 18)
            document.body.appendChild(document.createElement("br"));
    }

    document.body.appendChild(document.createElement("br"));

    textArea = document.createElement("textarea");
    textArea.style.width = "75%";
    document.body.appendChild(textArea);

    document.body.appendChild(document.createElement("br"));

    var clearButton = document.createElement("button");
    clearButton.innerText = "Clear";
    clearButton.onclick = function() {textArea.value = "";};
    document.body.appendChild(clearButton);
}

window.onload = load;
